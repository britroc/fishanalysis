input = getArgument();
run("Bio-Formats Importer", "open=/HDD/brenton_lab/FISH/"+input+"/tile_x=7_y=7.ome.tiff autoscale color_mode=Default group_files open_all_series view=Hyperstack stack_order=XYCZT axis_1_number_of_images=7 axis_1_axis_first_image=1 axis_1_axis_increment=1 axis_2_number_of_images=7 axis_2_axis_first_image=1 axis_2_axis_increment=1 contains=[] name=/HDD/brenton_lab/FISH/"+input+"/tile_x=<1-7>_y=<1-7>.ome.tiff");
run("Stack to Hyperstack...", "order=xyczt(default) channels=4 slices=5 frames=49 display=Color");
run("Bio-Formats Exporter", "save=/HDD/brenton_lab/FISH/"+input+".ome export compression=Uncompressed");
run("Close All");
